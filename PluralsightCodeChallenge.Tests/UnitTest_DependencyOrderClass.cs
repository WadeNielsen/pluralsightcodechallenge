using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PluralsightCodeChallenge.Exceptions;

namespace PluralsightCodeChallenge.Tests
{
    [TestClass]
    public class UnitTest_DependencyOrderClass
    {
        private readonly DependencyOrderService _dependencyOrderService;

        public UnitTest_DependencyOrderClass()
        {
            _dependencyOrderService = new DependencyOrderService();
        }

        /// <summary>
        /// Tests the valid example1, given in the exercise.
        /// </summary>
        [TestMethod]
        public void Test_ValidExample1()
        {
            string[] installPackageList = { "KittenService: CamelCaser",
                                            "CamelCaser: " };
            string result = _dependencyOrderService.GetDependencyInstallOrder(installPackageList);

            Assert.AreEqual("CamelCaser, KittenService", result);
        }

        /// <summary>
        /// Tests the valid example2, given in the exercise.
        /// </summary>
        [TestMethod]
        public void Test_ValidExample2()
        {
            string[] installPackageList = { "KittenService: ",
                                            "Leetmeme: Cyberportal",
                                            "Cyberportal: Ice",
                                            "CamelCaser: KittenService",
                                            "Fraudstream: Leetmeme",
                                            "Ice: " };
            string result = _dependencyOrderService.GetDependencyInstallOrder(installPackageList);

            Assert.AreEqual("KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream", result);
        }

        /// <summary>
        /// Test for null argument exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Test_NullArgument()
        {
            _dependencyOrderService.GetDependencyInstallOrder(null);
        }

        /// <summary>
        /// Tests for cyclic dependencies from the top level package.
        /// invalid example given in the exercise.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(CyclicDependencyException))]
        [Timeout(1000)]
        public void Test_InvalidExample1()
        {
            string[] installPackageList = { "KittenService: ",
                                            "Leetmeme: Cyberportal",
                                            "Cyberportal: Ice",
                                            "CamelCaser: KittenService",
                                            "Fraudstream: ",
                                            "Ice: Leetmeme" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for cyclic dependencies at lower levels.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(CyclicDependencyException))]
        [Timeout(1000)]
        public void Test_InvalidExample2()
        {
            string[] installPackageList = { "KittenService: ",
                                            "Leetmeme: Cyberportal",
                                            "Cyberportal: Ice",
                                            "CamelCaser: KittenService",
                                            "Fraudstream: ",
                                            "Ice: Cyberportal" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for null item in array.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Test_InvalidExample3()
        {
            string[] installPackageList = { "KittenService: ",
                                            "Leetmeme: Cyberportal",
                                            "Cyberportal: Ice",
                                            "CamelCaser: KittenService",
                                            null,
                                            "Ice: Cyberportal" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for duplicate package name exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DuplicatePackageNameException))]
        public void Test_InvalidExample4()
        {
            string[] installPackageList = { "Cyberportal: ",
                                            "Cyberportal: Ice" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for format exception thrown with no colon.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PackageStringFormatException))]
        public void Test_InvalidExample5()
        {
            string[] installPackageList = { "Cyberportal " };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for format exception thrown with no package name.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PackageStringFormatException))]
        public void Test_InvalidExample6()
        {
            string[] installPackageList = { " : Cyberportal" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }

        /// <summary>
        /// Test for format exception thrown with empty string
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(PackageStringFormatException))]
        public void Test_InvalidExample7()
        {
            string[] installPackageList = { "" };
            _dependencyOrderService.GetDependencyInstallOrder(installPackageList);
        }
    }
}
