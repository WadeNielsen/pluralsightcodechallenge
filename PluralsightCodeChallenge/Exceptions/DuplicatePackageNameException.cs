﻿using System;
namespace PluralsightCodeChallenge.Exceptions
{
    public class DuplicatePackageNameException : Exception
    {
        public DuplicatePackageNameException()
        {
        }

        public DuplicatePackageNameException(string message)
            : base(message)
        {
        }

        public DuplicatePackageNameException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
