﻿using System;
namespace PluralsightCodeChallenge.Exceptions
{
    public class PackageStringFormatException : Exception
    {
        public PackageStringFormatException()
        {
        }

        public PackageStringFormatException(string message)
            : base(message)
        {
        }

        public PackageStringFormatException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
