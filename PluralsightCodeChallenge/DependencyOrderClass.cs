﻿using System;
using System.Collections.Generic;
using System.Text;
using PluralsightCodeChallenge.Exceptions;

namespace PluralsightCodeChallenge
{
    public class DependencyOrderService
    {
        public DependencyOrderService()
        {
        }

        /// <summary>
        /// Given a list of install packages and thier dependencies
        /// this function returns the dependency install order.
        /// </summary>
        /// <returns>The dependency install order as a comma separated list</returns>
        /// <param name="installPackages">Arrary of packages and dependencies. Format = ["PackageName: Dependency", "PackageName2: Dependency2"]</param>
        public string GetDependencyInstallOrder(string[] installPackages)
        {
            // Dictionary for holding and organizing package names with their dependecies.
            Dictionary<string, string> installPackageDic = new Dictionary<string, string>();

            // Check parameter inputs.
            if (installPackages is null)
            {
                throw new ArgumentNullException("installPackages is null");
            }

            // Check inputs and fill the installPackageDictionary
            foreach (string packageStr in installPackages)
            {
                // To be filled with the current package name.
                string packageName;

                // To be filled with the dependency for this package.
                string dependency;

                // Check the individual array item for null.
                if (packageStr is null)
                {
                    throw new ArgumentException("Item in installPackages parameter is null");
                }

                // Get index of colon.
                int colonIndex = packageStr.IndexOf(':');

                // Check if colon was found.
                if (colonIndex < 0)
                {
                    throw new PackageStringFormatException("Colon not found.");
                }

                // Get packageName and dependency.
                packageName = packageStr.Substring(0, colonIndex).Trim();
                dependency = packageStr.Substring(colonIndex + 1).Trim();

                // Check package name
                if (string.IsNullOrEmpty(packageName))
                {
                    throw new PackageStringFormatException("Package name field empty.");
                }

                // Check for duplicates
                if (installPackageDic.ContainsKey(packageName))
                {
                    throw new DuplicatePackageNameException("Package name included more than once.");
                }

                // For further processing always set no dependency to null.
                if (string.IsNullOrEmpty(dependency))
                {
                    dependency = null;
                }

                // Add package name with dependency to dictionary.
                installPackageDic[packageName] = dependency;
            }

            // Keeps track of the current set of dependencies when traversing dependencies, used to efficiently find cycles.
            HashSet<string> currentDependencySet = new HashSet<string>();
            // Keeps track of the install package order, and packages already added to the install list.
            List<string> orderedInstallPackageList = new List<string>();

            // Loop over every install package and get the install order.
            foreach (var kvp in installPackageDic)
            {
                // Clear the hash set before starting.
                currentDependencySet.Clear();

                // Add package name and dependencies to the output list.
                string topLevelPackageName = kvp.Key;
                AddPackageAndDependencies(topLevelPackageName);

                // Helper function to get dependencies for dependencies.
                void AddPackageAndDependencies(string packageName)
                {
                    // If output list does not contain the package name, add it, and its dependencies.
                    if (!orderedInstallPackageList.Contains(packageName))
                    {
                        // Get the dependency.
                        string dependency = installPackageDic[packageName];

                        // Add the package name to our dependency set.
                        currentDependencySet.Add(packageName);

                        // If this package has dependencies, add them.
                        if (dependency != null)
                        {
                            // Check for cyclic dependencies.
                            if(currentDependencySet.Contains(dependency))
                            {
                                throw new CyclicDependencyException();
                            }

                            // Add the dependency(s).
                            AddPackageAndDependencies(dependency);
                        }

                        // Finally after adding all the dependencies add the package name.
                        orderedInstallPackageList.Add(packageName);
                    }
                }
            }

            // Format install package list to comma separated list.
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < orderedInstallPackageList.Count; ++i)
            {
                sb.Append(orderedInstallPackageList[i]);

                if (i < orderedInstallPackageList.Count - 1)
                {
                    sb.Append(", ");
                }
            }

            return sb.ToString();
        }
    }
}
